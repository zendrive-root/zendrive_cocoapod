//
//  ZendriveSwiftDebugLoggger.swift
//  ZendriveSDKSwift
//
//  Created by Sumit Bangarwa on 09/02/24.
//  Copyright © 2024 Zendrive Inc. All rights reserved.
//

import Foundation
import ZendriveSDK

/**
 * Utility class which helps in uploading log and Debug data required for
 * debugging Zendrive Debug related issues.
 */

@objc public final class ZendriveSwiftDebugLoggger: NSObject {

    /**
     * Logs a message with a specified log level, class name, and method name.
     *
     * This method is used to log messages with different log levels along with
     * class name and method name information. The logged messages can help in debugging
     * and understanding the flow of the application.
     *
     * @param level The log level indicating the severity of the message. Possible values
     *              are ZDRLogLevelError, ZDRLogLevelWarn, ZDRLogLevelInfo, ZDRLogLevelDebug,
     *              and ZDRLogLevelVerbose.
     * @param message The message to be logged.
     * @param className The name of the class where the log message originates from.
     * @param methodName The name of the method where the log message originates from.
     */

    public static func addDebugLogWith(_ level: ZendriveDebugLogLevel,
                                       message: String,
                                       file: StaticString = #file,
                                       method: StaticString = #function) {
        ZendriveDebugLogger.addDebugLog(with: level,
                                        message: message,
                                        className: String(describing: file),
                                        methodName: String(describing: method))
    }

    /**
     * Logs a message with a specified error log, class name, and method name.
     *
     * This method is used to log messages with error log levels along with
     * class name and method name information. The logged messages can help in debugging
     * and understanding the flow of the application.
     *
     * @param message The message to be logged.
     * @param className The name of the class where the log message originates from.
     * @param methodName The name of the method where the log message originates from.
     */
    public static func logError(_ message: String,
                                file: StaticString = #file,
                                method: StaticString = #function) {
        ZendriveDebugLogger.addDebugLog(with: .error,
                                        message: message,
                                        className: String(describing: file),
                                        methodName: String(describing: method))
    }

    /**
     * Logs a message with a specified info log, class name, and method name.
     *
     * This method is used to log messages with info log levels along with
     * class name and method name information. The logged messages can help in debugging
     * and understanding the flow of the application.
     *
     * @param message The message to be logged.
     * @param className The name of the class where the log message originates from.
     * @param methodName The name of the method where the log message originates from.
     */

    public static func logInfo(_ message: String,
                               file: StaticString = #file,
                               method: StaticString = #function) {
        ZendriveDebugLogger.addDebugLog(with: .info,
                                        message: message,
                                        className: String(describing: file),
                                        methodName: String(describing: method))
    }

    /**
     * Logs a message with a specified warning log, class name, and method name.
     *
     * This method is used to log messages with warning log levels along with
     * class name and method name information. The logged messages can help in debugging
     * and understanding the flow of the application.
     *
     * @param message The message to be logged.
     * @param className The name of the class where the log message originates from.
     * @param methodName The name of the method where the log message originates from.
     */
    public static func logWarn(_ message: String,
                               file: StaticString = #file,
                               method: StaticString = #function) {
        ZendriveDebugLogger.addDebugLog(with: .warn,
                                        message: message,
                                        className: String(describing: file),
                                        methodName: String(describing: method))
    }

    /**
     * Logs a message with a specified debug log, class name, and method name.
     *
     * This method is used to log messages with debug log levels along with
     * class name and method name information. The logged messages can help in debugging
     * and understanding the flow of the application.
     *
     * @param message The message to be logged.
     * @param className The name of the class where the log message originates from.
     * @param methodName The name of the method where the log message originates from.
     */
    public static func logDebug(_ message: String,
                                file: StaticString = #file,
                                method: StaticString = #function) {
        ZendriveDebugLogger.addDebugLog(with: .debug,
                                        message: message,
                                        className: String(describing: file),
                                        methodName: String(describing: method))
    }

    /**
     * Logs a message with a specified verbose log, class name, and method name.     *
     * This method is used to log messages with verbose log levels along with
     * class name and method name information. The logged messages can help in debugging
     * and understanding the flow of the application.
     *
     * @param message The message to be logged.
     * @param className The name of the class where the log message originates from.
     * @param methodName The name of the method where the log message originates from.
     */
    public static func logVerbose(_ message: String,
                                  file: StaticString = #file,
                                  method: StaticString = #function) {
        ZendriveDebugLogger.addDebugLog(with: .verbose,
                                        message: message,
                                        className: String(describing: file),
                                        methodName: String(describing: method))
    }

    /**
     * Add a Debug Data with Key and Value
     *
     * This method is used to add a debug data with keys Dictionary along with Value Dictionary
     * The Debug Data can help in debugging and understanding the application.
     *
     * @param keys  the dictionary that has to added in key dictionary
     * @param values the data that has to saved in debug data
     */
    public static func saveDebugData(keys: [AnyHashable: Any],
                                     values: [AnyHashable: Any]) {
        ZendriveDebugLogger.saveDebugData(withKeys: keys, values: values)
    }
}
