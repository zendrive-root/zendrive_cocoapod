//
//  PausedReason.swift
//  ZendriveSDKSwift
//
//  Created by Pushya Mitra on 09/03/22.
//  Copyright © 2022 Zendrive Inc. All rights reserved.
//

import Foundation
import ZendriveSDK

/// Wrapper for adding the sdk pause reason
@objc(ZDPausedReason) public final class PausedReason: NSObject {

    /// This enum determines the reason to pause the trip recording
    @objc public var pausedReason: ZendrivePausedReasonEnum

    /// This method is used to add the pause reason 
    @objc public convenience init(pausedReason: NSInteger) {
        let objcPausedReason =  ZendrivePausedReason(zdrzdrPauseTrackingReason: pausedReason)
        self.init(with: objcPausedReason)
    }

    init(with objcPausedReason: ZendrivePausedReason) {
        self.pausedReason = objcPausedReason.pausedReason
    }
}
