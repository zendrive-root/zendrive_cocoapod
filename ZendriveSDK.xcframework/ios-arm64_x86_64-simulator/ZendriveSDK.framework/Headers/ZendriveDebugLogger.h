//
//  ZendriveDebugLogger.h
//  ZendriveSDK
//
//  Created by Sumit Bangarwa on 09/02/24.
//  Copyright © 2024 Zendrive Inc. All rights reserved.
//


/**
 * Utility class which helps in uploading data required for
 * debugging Zendrive  related issues.
 **/
@interface ZendriveDebugLogger : NSObject

/**
 * Enum representing log levels for SDK Log.
 **/
typedef NS_ENUM(int, ZendriveDebugLogLevel){
    /**
     * Used for critical errors.
     */
    ZendriveDebugLogLevelError = 0,

    /**
     * Used for non-critical issues that may need attention.
     */
    ZendriveDebugLogLevelWarn,

    /**
     * Used for general informational messages.
     */
    ZendriveDebugLogLevelInfo,

    /**
     * Used for messages helpful for debugging purposes.
     */
    ZendriveDebugLogLevelDebug,
    
    /**
     * Used for highly detailed and verbose messages.
     */
    ZendriveDebugLogLevelVerbose
};


/**
 * Logs a message with a specified log level, class name, and method name.

 * This method is used to log messages with different log levels along with
 * class name and method name information. The logged messages can help in debugging
 * and understanding the flow of the application.
 *
 * @param level The log level indicating the severity of the message. Possible values
 *              are ZDRLogLevelError, ZDRLogLevelWarn, ZDRLogLevelInfo, ZDRLogLevelDebug,
 *              and ZDRLogLevelVerbose.
 * @param message The message to be logged.
 * @param className The name of the class where the log message originates from.
 * @param methodName The name of the method where the log message originates from.
 */
+ (void)addDebugLogWithLevel:(ZendriveDebugLogLevel)level
                     message:(NSString * _Nonnull)message
                   className:(NSString * _Nullable)className
                  methodName:(NSString * _Nullable)methodName;


/**
 * Add a Debug Data with Key and Value
 *
 * This method is used to add a debug data with keys Dictionary along with Value Dictionary
 * The Debug Data can help in debugging and understanding the application.
 *
 * @param keys  the dictionary that has to added in key dictionary
 * @param values the  data that has to saved in debug data
 */
+ (void)saveDebugDataWithKeys:(NSDictionary * _Nonnull)keys
                     values:(NSDictionary * _Nonnull)values;

@end




// Define macros to make logging easier
#define ZenriveDebugLog(level, frmt, ...)   [ZendriveDebugLogger addDebugLogWithLevel:level \
                                                                        message:[NSString stringWithFormat:frmt, ##__VA_ARGS__] \
                                                                      className:NSStringFromClass([self class]) \
                                                                     methodName:NSStringFromSelector(_cmd)]

// High level logs
#define ZenriveDebugLogError(frmt, ...)      ZendriveDebugLogger(ZendriveDebugLogLevelError, frmt, ##__VA_ARGS__)
#define ZenriveDebugLogWarn(frmt, ...)       ZendriveDebugLogger(ZendriveDebugLogLevelWarn, frmt, ##__VA_ARGS__)
#define ZenriveDebugLogInfo(frmt, ...)       ZendriveDebugLogger(ZendriveDebugLogLevelInfo, frmt, ##__VA_ARGS__)

// Debug logs -- not used in release
#define ZenriveDebugLogDebug(frmt, ...)      ZendriveDebugLogger(ZendriveDebugLogLevelDebug, frmt, ##__VA_ARGS__)
#define ZenriveDebugLogVerbose(frmt, ...)    ZendriveDebugLogger(ZendriveDebugLogLevelVerbose, frmt, ##__VA_ARGS__)
