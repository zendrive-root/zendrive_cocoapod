//
//  ZendrivePausedReason.h
//  ZendriveSDK
//
//  Created by Pushya Mitra on 09/03/22.
//  Copyright © 2022 Zendrive Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifndef ZendrivePausedReason_h
#define ZendrivePausedReason_h


/**
 * The value return from `ZendrivePausedReasonEnum`.
 * Enum representing different reason for pausing the trips recording
 **/
typedef NS_ENUM(int, ZendrivePausedReasonEnum) {
    
    /**
     * When pause reason is user manually pause the recording of trip
     */
    ZendrivePausedReasonUser,
    
    /**
     * When pause reason is out of business hours
     */
    ZendrivePausedReasonBusinessHours,
    
    /**
     * When pause reason is not start detection the trip
     */
    ZendrivePausedReasonNotPaused
};

/**
 * Set the different reasonfor pausing the trips recording
 */
@interface ZendrivePausedReason : NSObject

/**
 * This enum determines the reason to pause the trip recording
 */
@property (nonatomic, assign) ZendrivePausedReasonEnum pausedReason;

/**
 * This method is used to add the pause reason
 */
-(id _Nonnull)initWithZDRZDRPauseTrackingReason:(NSInteger)pausedReason;

@end

#endif /* ZendrivePausedReason_h */
